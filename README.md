# Actian setup guide

### Environment
- Mac
- Ubuntu 16.04.2 on Virtual Box
  - requires 60GB+ storage for benchmark 
    - 30GB for data generation
    - others for database
    - [storage](http://imgur.com/a/DSv4D)
- Windows on Virtual Box
  - for GUI client test

### Download and register
- Actian Vector Community Edition: [here](https://www.actian.com/lp/vector-community-edition/)
- Register Actian ID: [here](http://supportservices.actian.com/user/register.php)

### Server install on linux
- [Getting Started](http://docs.actian.com/vector/5.0/index.html#page/GetStart%2FInstalling_Vector_for_Linux.htm%23)
- apt-get install libaio1
- wget [url]/actian-vector-5.0.0-405-community-linux-x86_64.tgz
  - download link should be in email
  - http://imgur.com/a/xT1SH
- tar xvzf actian-vector-5.0.0-405-community-linux-x86_64.tgz
- ./install.sh -express
  - express install - no interaction required
  - [install](http://imgur.com/a/V4SJ5)

### Benchmark
#### prepare database
- create password to login 'actian' user
  - 'sudo passwd actian'
- su actian
  - I got error when generating data using ssh connection
- get DBT-3 package
  - wget http://downloads.sourceforge.net/project/osdldbt/dbt3/1.9/dbt3-1.9.tar.gz
  - cd dbt3-1.9/src/dbgen
  - make
- generate 30GB data
  - dbgen -vfF -s 30
  - take some time
- create a database
  - createdb dbtest
- cretae a vector table [here](http://docs.actian.com/vector/5.0/index.html#page/User%2FCreate_a_Vector_Table.htm%23)
- load the data into database
  - vwload --table lineitem --fdelim "|" dbtest lineitem.tbl
#### run benchmark from windows client
- install client downloaded from email link
- connect to server (linux in virtualbox)
- run reporting query: [here](http://docs.actian.com/vector/5.0/index.html#page/User%2FRun_a_Reporting_Query.htm%23)
  - [result](http://imgur.com/a/dl0uE)


